package stsg.code;

public class STSGFactory {
	public static ISTSG stsgproducer(STSGType type) {
		if (type == STSGType.COMPRESSION) {
			return new stsg.code.sentence_compression.CompressionSTSGImpl();
		} else if (type == STSGType.MOVEMENT) {
			return new stsg.code.sentence_movement.MovementSTSGImpl();
		}
		return null;
	}
}
